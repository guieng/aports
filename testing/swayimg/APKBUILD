# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=swayimg
pkgver=1.6
pkgrel=0
pkgdesc="Image viewer for Sway"
url="https://github.com/artemsen/swayimg"
arch="all"
license="MIT"
makedepends="
	bash-completion
	cairo-dev
	giflib-dev
	json-c-dev
	libavif-dev
	libexif-dev
	libjpeg-turbo-dev
	libjxl-dev
	libxkbcommon-dev
	meson
	wayland-dev
	wayland-protocols
	"
subpackages="
	$pkgname-full
	$pkgname-doc
	$pkgname-bash-completion
	"
source="https://github.com/artemsen/swayimg/archive/v$pkgver/swayimg-$pkgver.tar.gz
	system-level-config.patch
	"
options="!check"  # no tests provided

case "$CARCH" in
	riscv64 | s390x) _librsvg='disabled';;  # librsvg is not available
	*) _librsvg='enabled'; makedepends="$makedepends librsvg-dev";;
esac

build() {
	msg 'Building basic variant'
	_build output \
		-Davif=disabled \
		-Djxl=disabled \
		-Dsvg=disabled

	msg 'Building full variant'
	_build output-full \
		-Dbash=disabled \
		-Dman=false
}

_build() {
	local outdir=$1; shift

	abuild-meson \
		-Davif=enabled \
		-Dgif=enabled \
		-Djpeg=enabled \
		-Djxl=enabled \
		-Dpng=enabled \
		-Dsvg=$_librsvg \
		-Dwebp=disabled \
		-Dexif=enabled \
		-Dbash=enabled \
		-Dversion=$pkgver \
		"$@" \
		. "$outdir"
	meson compile -j ${JOBS:-0} -C "$outdir"
}

package() {
	pkgdesc="$pkgdesc - with support for basic formats"
	provider_priority="100"  # highest (other provider is swayimg-full)

	DESTDIR="$pkgdir" meson install --no-rebuild -C output
	install -D -m644 extra/swayimgrc "$pkgdir"/etc/xdg/$pkgname/config
}

full() {
	pkgdesc="$pkgdesc - with support for all formats"
	provides="$pkgname=$pkgver-r$pkgrel"
	provider_priority="10"  # lowest (other provider is swayimg)

	cd "$builddir"

	DESTDIR="$subpkgdir" meson install --no-rebuild -C output-full
	install -D -m644 extra/swayimgrc "$subpkgdir"/etc/xdg/$pkgname/config
}

sha512sums="
5b7321485185f5e23d4a3a00613ef164db2e1fea3e2231d3440895474c18debee4f0918ffe9a6e0a515457b6d6f92b8950ed6e114fcf6d317c8f526df557cdb4  swayimg-1.6.tar.gz
5f74235c9f7ec21b344206e1f29601065f6a7b711977ef180fb71409b181c490b0e8735d00ec2ae41dc892635983c60d8f959a775efcf047b4c2c975b9067f8a  system-level-config.patch
"
