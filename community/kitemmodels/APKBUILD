# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kitemmodels
pkgver=5.91.0
pkgrel=0
pkgdesc="Models for Qt Model/View system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-only AND LGPL-2.0-or-later"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qtdeclarative-dev
	qt5-qttools-dev
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kitemmodels-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# kdescendantsproxymodel_smoketest and kdescendantsproxymodeltest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kdescendantsproxymodel(_smoketest|test)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7926ac4e0ba816a09d5257372f9941e0e2581490f68568705c2e186bbc5ff71507b05575d1ca31578f73da2999427651922885463eea5336ccb6086a45b4f176  kitemmodels-5.91.0.tar.xz
"
