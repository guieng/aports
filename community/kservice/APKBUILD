# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kservice
pkgver=5.91.0
pkgrel=0
pkgdesc="Advanced plugin and service introspection"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="kconfig-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev ki18n-dev"
makedepends="$depends_dev extra-cmake-modules kdoctools-dev doxygen qt5-qttools-dev flex-dev bison"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kservice-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# ksycoca_xdgdirstest, kmimeassociationstest kapplicationtradertest and ksycocathreadtest are broken
	# ksycocatest requires running X
	local skipped_tests="("
	local tests="
		kapplicationtrader
		kmimeassociations
		ksycoca
		ksycoca_xdgdirs
		ksyscocathread
		"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
c7dd3ba74631e3af44ab5cd79605fbeb87afa20360cdc832da83bdc9fadfbd2e377689bb28857584ac7ec3599ef3907f7dc9a4d206470544f790c6ccdfe432a7  kservice-5.91.0.tar.xz
"
