# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=obs-studio
pkgver=27.2.0
pkgrel=0
pkgdesc="Free and open source software for live streaming and screen recording"
url="https://obsproject.com/"
arch="all"
license="GPL-2.0-or-later"
options="!check"
makedepends="cmake ffmpeg-dev libxinerama-dev
	qt5-qtbase-dev qt5-qtx11extras-dev qt5-qtsvg-dev x264-dev fontconfig-dev
	libxcomposite-dev freetype-dev libx11-dev mesa-dev curl-dev
	pulseaudio-dev jack-dev alsa-lib-dev fdk-aac-dev speexdsp-dev
	v4l-utils-dev jansson-dev eudev-dev swig mbedtls-dev python3-dev
	wayland-dev pipewire-dev sndio-dev libxkbcommon-dev pciutils-dev"
subpackages="$pkgname-dev"
source="https://github.com/obsproject/obs-studio/archive/$pkgver/obs-studio-$pkgver.tar.gz"

# armhf, s390x and riscv64 blocked by librsvg -> vlc
case $CARCH in
	armhf|s390x|riscv64)
		;;
	*)
		makedepends="$makedepends vlc-dev"
		;;
esac

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DUNIX_STRUCTURE=1 \
		-DBUILD_BROWSER=OFF \
		-DBUILD_VST=OFF \
		-DOBS_VERSION_OVERRIDE=$pkgver \
		-DENABLE_PIPEWIRE=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0f56e12d429c3208a9d200d085606dd401952a9a37d03ecf70e46d932fb8e907906b53c3a4ea7c9e10a3cbb3001bbd569163457d5bcb41eb8cce984b3c2b4d90  obs-studio-27.2.0.tar.gz
"
