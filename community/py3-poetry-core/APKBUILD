# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-poetry-core
_pkgname=poetry-core
pkgver=1.0.7
pkgrel=4
pkgdesc="PEP 517 build backend implementation for Poetry"
url="https://github.com/python-poetry/poetry-core"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-build py3-pip"
checkdepends="py3-pytest py3-pytest-mock py3-virtualenv python3-dev"
source="$_pkgname-$pkgver.tar.gz::https://github.com/python-poetry/poetry-core/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

check() {
	# skip test that is incompatible with the .gitignore file in the aports tree (src entry)
	pytest tests \
		--deselect tests/masonry/builders/test_sdist.py::test_includes_with_inline_table
	PYTHONPATH="$PWD" pytest --integration tests/integration
}

build() {
	# poetry-core excludes files specified in .gitignore
	# fixed by setting GIT_DIR away from the aports tree's top-level directory
	GIT_DIR="$PWD" python3 -m build --no-isolation --skip-dependency-check --wheel
}

package() {
	local sitedir=$(python3 -c "import site; print(site.getsitepackages()[0])")

	python3 -m pip install --root "$pkgdir" --no-deps --no-warn-script-location \
		dist/poetry_core-$pkgver-py2.py3-none-any.whl

	# remove vendored versions of installed modules
	rm -r "$pkgdir/$sitedir"/poetry/core/_vendor

	install -Dm644 poetry/core/json/schemas/poetry-schema.json \
		"$pkgdir/$sitedir"/poetry/core/json/schemas/poetry-schema.json
	install -Dm644 poetry/core/spdx/data/licenses.json \
		"$pkgdir/$sitedir"/poetry/core/spdx/data/licenses.json
}

sha512sums="
40a8b17ae7c51135eb5f96ec76426f075075de7dafff8ab828fe3b47d13759856a7fd2e7504c1216c4f942befde2c6d42c3976978202ce7aa224e966162bd8e5  poetry-core-1.0.7.tar.gz
"
