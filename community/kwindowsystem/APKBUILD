# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwindowsystem
pkgver=5.91.0
pkgrel=0
pkgdesc="Access to the windowing system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="MIT AND (LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="qt5-qtx11extras-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev doxygen libxrender-dev xcb-util-keysyms-dev xcb-util-wm-dev"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwindowsystem-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# kwindowsystem-kwindowinfox11test hangs
	# kwindowsystem-kwindowsystemx11test, kwindowsystem-kwindowsystem_threadtest and kwindowsystem-netrootinfotestwm are broken
	# kwindowsystem-netwininfotestwm is broken on s390x
	local skipped_tests="kwindowsystem-("
	local tests="
		kwindowinfox11test
		kwindowsystemx11test
		kwindowsystem_threadtest
		netrootinfotestwm
		"
	case "$CARCH" in
		s390x) tests="$tests
			netwininfotestwm
			"
	esac
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)"
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
29b9fe96e13eb0a22d6c4bd457981f5b3c094799c6fb01dccdbb93581ab4c3ce72d5918018bd7a3b9e69ad94d433d5e910c1680d6bc155f037e9a8b869d31310  kwindowsystem-5.91.0.tar.xz
"
